﻿using System;
using Newtonsoft.Json;
using System.Diagnostics;

namespace OTUS10
{
    class Program
    {
        static void Main(string[] args)
        {
            int cnt = 1000000;

            RunCSVSerialize(cnt);
            RunJsonSerialize(cnt);

        }

        private static void RunCSVSerialize(int cnt)
        {
            var serializeSW = new Stopwatch();
            var deserializeSW = new Stopwatch();

            var instance = F.Get();
            for (var i = 0; i < cnt; i++)
            {
                serializeSW.Start();
                var csv = CustSerializer.SerializeToCsv(instance);
                serializeSW.Stop();

                deserializeSW.Start();
                var deserialized = CustSerializer.DeserializeFromCsv(csv);
                deserializeSW.Stop();
                if (instance.Equals((F)deserialized))
                {
                    Console.WriteLine($"Несовпадения наборов, прогон {i}");
                }
            }

            Console.WriteLine($"Количество итераций: {cnt}");
            Console.WriteLine($"CSV сериалиализация: {serializeSW.ElapsedMilliseconds} мс");
            Console.WriteLine($"CSV десериалиализация: {deserializeSW.ElapsedMilliseconds} мс");
        }

        private static void RunJsonSerialize(int cnt)
        {
            var serializeSW = new Stopwatch();
            var deserializeSW = new Stopwatch();

            var instance = F.Get();
            for (var i = 0; i < cnt; i++)
            {
                serializeSW.Start();
                var csvContent = JsonConvert.SerializeObject(instance);
                serializeSW.Stop();

                deserializeSW.Start();
                var deserialized = JsonConvert.DeserializeObject(csvContent, typeof(F));
                deserializeSW.Stop();
                if (instance.Equals((F)deserialized))
                {
                    Console.WriteLine($"Несовпадения наборов, прогон {i}");
                }
            }

            Console.WriteLine($"Количество итераций: {cnt}");
            Console.WriteLine($" Json сериалиализация: {serializeSW.ElapsedMilliseconds} мс");
            Console.WriteLine($" Json десериалиализации: {deserializeSW.ElapsedMilliseconds} мс");
        }


    }
}